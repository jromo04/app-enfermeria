import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import  swal  from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  
  user = {
    username: '',
    password: ''
  }
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  login(){
    this.authService.login(this.user).subscribe(res => {
      if (!res.token){
        swal.fire({
          title: '¡Error!',
          text: 'Usuario y/o contraseña incorrectos',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      } else {
        this.router.navigate(['/dashboard']);
      }
    })
  }
}
