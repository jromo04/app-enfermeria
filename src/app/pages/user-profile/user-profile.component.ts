import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { IngresosService } from '../../services/ingresos.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  public defaultImg = 'assets/img/theme/default-profile.png';
  public users: any[];
  public safe_photo: any;
  public user: any = {
    id_user: '',
    unique_id: '',
    name: '',
    first_ln: '',
    second_ln: '',
    id_course: '',
    course: {id_course: '', name: ''},
    email: '',
    phone: '',
    phone2: '',
    blood_type: '',
    photo: '',
    father: {id_user: '', name: ''},
    mother: {id_user: '', name: ''},
    allergies: []
  }
  public ingresos: any[] = [];

  constructor(private userService: UsersService,
              private ingresosService: IngresosService,
              private sanitizer: DomSanitizer,
              private urlRoute: ActivatedRoute) { }

  ngOnInit() {
    this.urlRoute.params.subscribe(params => {
      this.getUserById(params['id_user']);
    })
  }

  getUserById(id_user){
    this.userService.getById(id_user).subscribe(user => {
      this.user = user;
      this.safe_photo = user.photo ? this.sanitizer.bypassSecurityTrustStyle(`url(${user.photo})`) : `url(${this.defaultImg})`;
      this.getIngresos(user.id_user);
      console.log(this.user);
    })
  }

  getIngresos(user){
    this.ingresosService.getByUser(user).subscribe(ingresos =>{
      console.log(ingresos);
      this.ingresos = ingresos;
    })
  }

  onPhotoChange(evt: any){
    if (evt.target.files && evt.target.files[0]) {
      const reader: FileReader = new FileReader();
      
      reader.readAsDataURL(evt.target.files[0]); // read file as data url
      
      reader.onload = (evt: any) => { // called once readAsDataURL is completed
        this.user.photo = evt.target.result;
        this.safe_photo = this.sanitizer.bypassSecurityTrustStyle(`url(${evt.target.result})`);
      }
    }
  }

  removePhoto(){

  }

}
