import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UsersService } from './../../services/users.service';
import { ReportsService } from './../../services/reports.service';
import html2pdf from 'html2pdf.js';
import { NgxSpinnerService } from 'ngx-spinner';
import  swal  from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'app-accident-report',
  templateUrl: './accident-report.component.html',
  styleUrls: ['./accident-report.component.scss']
})
export class AccidentReportComponent implements OnInit {

  @ViewChild('accident_report') container: ElementRef;

  public today: any;
  public report: any;
  public students: any;
  public time;
  constructor(private userService: UsersService,
              private spinner: NgxSpinnerService,
              private reportService: ReportsService,
            ) { }

  ngOnInit() {
    this.today = new Date();
    this.getStudents();
    this.cleanModel()
  }

  getStudents(){
    let criteria = { id_profile: 2, status: "A" }
    this.userService.getByCriteria(criteria).subscribe(students => {
      this.students = students;
      this.students = students.map(std => {
        const student = {
          id_user: std.id_user,
          name: std.name,
          first_ln: std.first_ln,
          second_ln: std.second_ln,
          address: std.address,
          phone: std.phone,
          course: std.course
        }
        return student;
      })
    })
  }

  getStudentById(id){
    this.report.student = this.students.filter(std => { return std.id_user == id})[0]
    console.log(this.report.student)
  }

  async saveReport(){
    this.spinner.show();
    this.report.file = await this.generatePDF();
    this.report.time = new Date(this.today.setHours(this.report.time.hour, this.report.time.minute)).toISOString();
    this.report.time_notified = new Date(this.today.setHours(this.report.time_notified.hour, this.report.time_notified.minute)).toISOString();
    this.report.pick_time = new Date(this.today.setHours(this.report.pick_time.hour, this.report.pick_time.minute)).toISOString();
    console.log(this.report);
    this.reportService.create(this.report).subscribe(response => {
      this.spinner.hide();
      if (response.report){
        swal.fire({
          title: 'Success!',
          text: 'An accident report has been created successfully.',
          type: 'success',
          confirmButtonText: 'Ok'
        })
        this.cleanModel();
      } else {
        swal.fire({
          title: 'Error!',
          text: 'There was a problem creating the accident report.',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      }
    })
  }

  generatePDF(){
    window.scrollTo(0,0)
    return new Promise(resolve => {
      let content = this.container.nativeElement;
      var opt = {
        margin:       [0.5, 1],
        filename:     'accident-report.pdf',
        image:        { type: 'jpeg', quality: 1 },
        html2canvas:  { scale: 2 },
        jsPDF:        { unit: 'in', format: 'a3', orientation: 'portrait' }
      };

      // html2pdf().set(opt).from(content).save();

      html2pdf().set(opt).from(content).toPdf().output('datauristring')
        .then(pdfAsString => {
          resolve(pdfAsString);
        });
    });
  }

  formatDate(){
    let format = this.report.date_format;
    this.report.date = new Date(format.year+'-'+format.month+'-'+format.day).toISOString();
  }

  cleanModel(){
    this.report = {
      id_report: 0,
      id_student: 0,
      class: '',
      date: this.today.toISOString(),
      date_format: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate()},
      time: { hour: this.today.getHours(), minute: this.today.getMinutes() },
      accident_desc: '',
      injury_desc: '',
      witness1: '',
      witness2: '',
      pupil_doing: '',
      first_aide: '',
      parent_notified: false,
      how_notified: '',
      time_notified: { hour: this.today.getHours(), minute: this.today.getMinutes() },
      cust_stament: '',
      ambulance_called: false,
      supply_details: '',
      super_stament: '',
      child_picker: '',
      pick_time: { hour: this.today.getHours(), minute: this.today.getMinutes() },
      student: { }
    }
  }

}
