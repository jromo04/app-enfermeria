import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MedicinesService } from '../../services/medicines.service';
import  swal  from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";
import * as XLSX from 'xlsx';
import { UnitsService } from 'src/app/services/units.service';

@Component({
  selector: 'app-bodega',
  templateUrl: './bodega.component.html',
  styleUrls: ['./bodega.component.scss']
})
export class BodegaComponent implements OnInit {

  @ViewChild('medicineModal') medicineModal: NgbModal;

  public action = 0;  //   0: Crear; 1: Editar
  public medicines: any;
  public filter_medicines: any;
  public units: any;
  public medicine: any;
  public modalRef: any;
  public data: any;
  public importSuccess: boolean;
  public filename: string;

  constructor(private medicinesService: MedicinesService,
              private unitsService: UnitsService, 
              private modalService: NgbModal, 
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getMedicines();
    this.getUnits();
  }

  search(text) {
    const term = text.toLowerCase();
    this.filter_medicines = this.medicines.filter(m => {
      return m.cod_medicine.toString().includes(term)
          || m.name.concat(" ", m.description).toLowerCase().includes(term)
          || m.unit.name.toLowerCase().includes(term);
    });
  }

  getMedicines(){
    this.medicinesService.getAll().subscribe(medicines =>{
      this.medicines = medicines;
      this.filter_medicines = medicines;
    })
  }

  getUnits(){
    this.unitsService.getAll().subscribe(units =>{
      this.units = units;
    })
  }
  
  getMedicineById(id){
    this.open(this.medicineModal, 'lg');
    this.action = 1;
    this.medicine = this.medicines.find(el => {return el.id_medicine == id});
  }

  saveMedicine(){
    // console.log($('.selectpicker').val());
    if (this.action == 0){
      this.medicinesService.create(this.medicine).subscribe(medicine => {
        if(medicine.id_medicine){
          swal.fire({
            title: '¡Listo!',
            text: 'Producto creado correctamente',
            type: 'success',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
          this.cleanModel();
          this.modalRef.close();
          this.getMedicines();
        } else {
          swal.fire({
            title: 'Error!',
            text: 'Error en la creación del producto',
            type: 'error',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
        }
      });
    } else {
      this.medicinesService.update(this.medicine).subscribe(medicine => {
        if(medicine[0] > 0){
          swal.fire({
            title: '¡Listo!',
            text: 'Producto editado correctamente',
            type: 'success',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
          this.cleanModel();
          this.modalRef.close();
          this.getMedicines();
        } else {
          swal.fire({
            title: 'Error!',
            text: 'Error en la edición del producto',
            type: 'error',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
        }
      });
    }
  }

  inactivateMedicine(id){
    this.medicinesService.inactivate(id).subscribe(response => {
      if (response[0] > 0){
        this.getMedicines(); 
      } else {
        swal.fire({
          title: '¡Error!',
          text: 'Ha ocurrido un error durante la inactivación del producto.',
          type: 'error',
          confirmButtonColor: '#5E72E4',
          confirmButtonText: 'Ok'
        })
      }
    })
  }

  onFileChange(evt: any) {
    
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length > 1) {
      swal.fire({
        title: 'Error al adjuntar',
        text: 'No puede seleccionar más de un archivo.',
        type: 'warning',
        confirmButtonColor: '#5E72E4',
        confirmButtonText: 'Ok'
      })
    } else {
      this.spinner.show();
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
  
        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
        /* save data */
        this.data = (XLSX.utils.sheet_to_json(ws, {header: ["cod_medicine", "name", "description", "unit", "stock"]}));
      };
      if (target.files[0]){
        reader.readAsBinaryString(target.files[0]);
        this.filename = target.files[0].name;
      }
      
      setTimeout(() => {
        this.spinner.hide();
        this.data.shift();
        this.validateImport();
      }, 1000);
      
    }
  }

  validateImport(){
    this.importSuccess = true;
    this.data.map(element => {
      let unit = this.units.find(u => {return u.name.trim() == element.unit.trim()})
      element.id_unit = unit ? unit.id_unit:0;
      element.bodega = {stock: element.stock};
      if (!unit) { element.invalid = true; this.importSuccess = false };

      return element;
    });

    if(!this.importSuccess){
      swal.fire({
        title: '¡Error!',
        text: 'Hubo un error en la lectura del archivo. Por favor verifique la información ingresada.',
        type: 'error',
        confirmButtonColor: '#5E72E4',
        confirmButtonText: 'Ok'
      })
    }
    console.log(this.data);
  }

  importMedicines(){
    this.medicinesService.importMedicines(this.data).subscribe(response => {
      if (response.success > 0) {
        swal.fire({
          title: '¡Listo!',
          text: `Se guardaron ${response.success} productos correctamente`,
          type: 'success',
          confirmButtonColor: '#5E72E4',
          confirmButtonText: 'Ok'
        })
        this.modalRef.close();
        this.data = [];
        this.getMedicines();
      } else {
        console.log(response.errors);
        swal.fire({
          title: '¡Error!',
          text: 'Ha ocurrido un error durante la creación de productos',
          type: 'error',
          confirmButtonColor: '#5E72E4',
          confirmButtonText: 'Ok'
        })
      }
    })
  }

  open(modal, modalSize) {
    this.cleanModel();
    this.action = 0;
    modalSize = modalSize ? modalSize:'sm';
    this.modalRef = this.modalService.open(modal, {size: modalSize, centered: true});
    this.modalRef.result.then((data) => {}, (reason) => { this.cleanModel() });
  }

  cleanModel(){
    this.medicine = {
      id_medicine: null,
      cod_medicine: null,
      name: null,
      description: null,
      id_unit: 0,
      photo: null,
      bodega: {}
    }
  }
}
