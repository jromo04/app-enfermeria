import { Component, OnInit, ViewChild, Injectable } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { UserProfilesService } from 'src/app/services/user-profiles.service';
import { CoursesService } from 'src/app/services/courses.service';
import { AllergiesService } from 'src/app/services/allergies.service';
import { NgbModal, NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import  swal  from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";

import * as XLSX from 'xlsx';
import { DomSanitizer } from '@angular/platform-browser';

declare var $: any;

const I18N_VALUES = {
  'es': {
    weekdays: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá', 'Do'],
    months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
  }
  // other languages you would support
};

@Injectable()
export class I18n {
  language = 'es';
}

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  constructor(private _i18n: I18n) {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return I18N_VALUES[this._i18n.language].months[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }
  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [NgbModal, I18n, {provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})
export class UsersComponent implements OnInit {

  @ViewChild('userModal') userModal: NgbModal;
  public defaultImg = 'assets/img/theme/default-profile.png';
  public action = 0;  //   0: Crear; 1: Editar
  public data: any;
  public today = new Date();
  public users: any[];
  public filter_users: any[];
  public modalRef: any;
  public modalTitle: string = '';
  public profiles: any[];
  public courses: any[];
  public allergies: any[];
  public teacher_courses: any[];
  public filename: string;
  public importSuccess: boolean;
  public safe_photo: any = this.sanitizer.bypassSecurityTrustStyle(`url(${this.defaultImg})`);
  public user: any = {
    id_user: '',
    unique_id: '',
    name: '',
    first_ln: '',
    second_ln: '',
    id_course: 0,
    email: '',
    phone: '',
    phone2: '',
    address: '',
    blood_type: 0,
    id_profile: 0,
    photo: null,
    birthdate: '',
    birthdateFormat: '',
    allergies: [],
    teacher_courses: []
  }

  constructor(private userService: UsersService, 
              private userProfileService: UserProfilesService, 
              private coursesService: CoursesService, 
              private allergiesService: AllergiesService, 
              private modalService: NgbModal,
              private spinner: NgxSpinnerService,
              private sanitizer: DomSanitizer
              ) { }

  ngAfterContentChecked() {
    $('.selectpicker').selectpicker();
  }
  
  ngOnInit() {
    this.getUsers();
    this.getUserProfiles();
    this.getCourses();
    this.getAllergies();
  }

  search(text) {
    const term = text.toLowerCase();
    this.filter_users = this.users.filter(user => {
      return user.name.concat(" ", user.first_ln).toLowerCase().includes(term)
          || user.email.toLowerCase().includes(term)
          || user.user_profile.name.toLowerCase().includes(term);
    });
  }

  getUsers(){
    this.userService.getAll().subscribe(users => {
      this.users = users;
      this.filter_users = users;
      console.log(users);
    })
  }

  getUserById(id_user){
    this.open(this.userModal, 'lg');
    this.action = 1;
    this.userService.getById(id_user).subscribe(user => {
      this.user.id_user = user.id_user;
      this.user.unique_id = user.unique_id;
      this.user.name = user.name;
      this.user.first_ln = user.first_ln;
      this.user.second_ln = user.second_ln;     
      this.user.id_course = user.id_course;
      this.user.email = user.email;
      this.user.phone = user.phone;
      this.user.phone2 = user.phone2;
      this.user.address = user.address;
      this.user.blood_type = user.blood_type;
      this.user.id_profile = user.id_profile;

      this.user.photo = user.photo;
      this.safe_photo = user.photo ? this.sanitizer.bypassSecurityTrustStyle(`url(${user.photo})`) : `url(${this.defaultImg})`;
      
      //Format birthdate to ngbDatePicker value
      let formatDtp = user.birthdate.split('-');
      this.user.birthdateFormat = {year: parseInt(formatDtp[0]), month: parseInt(formatDtp[1]), day: parseInt(formatDtp[2])};


      this.user.allergies = user.allergies.map(al => al.id_allergy);
      this.user.teacher_courses = user.courses.map(c => c.id_course);
      console.log(this.user);
    })
  }
  
  saveUser(){
    console.log($('.selectpicker').val());
    if (this.action == 0){
      this.userService.create(this.user).subscribe(user => {
        if(user){
          swal.fire({
            title: '¡Listo!',
            text: 'Usuario creado correctamente',
            type: 'success',
            confirmButtonText: 'Ok'
          })
          this.cleanModel();
          this.modalRef.close();
          this.getUsers();
        } else {
          swal.fire({
            title: 'Error!',
            text: 'Error en la creación del usuario',
            type: 'error',
            confirmButtonText: 'Ok'
          })
        }
      });
    } else {
      this.userService.update(this.user).subscribe(user => {
        if(user){
          swal.fire({
            title: '¡Listo!',
            text: 'Usuario editado correctamente',
            type: 'success',
            confirmButtonText: 'Ok'
          })
          this.cleanModel();
          this.modalRef.close();
          this.getUsers();
        } else {
          swal.fire({
            title: 'Error!',
            text: 'Error en la edición del usuario',
            type: 'error',
            confirmButtonText: 'Ok'
          })
        }
      });
    }
  }
  
  getUserProfiles(){
    this.userProfileService.getAll().subscribe(profiles => {
      this.profiles = profiles;
    })
  }
  
  getCourses(){
    this.coursesService.getAll().subscribe(courses => {
      this.courses = courses;
    })
  }

  getAllergies(){
    this.allergiesService.getAll().subscribe(allergies => {
      this.allergies = allergies;
    })
  }

  formatBirthday(){
    let format = this.user.birthdateFormat;
    this.user.birthdate = new Date(format.year+'-'+format.month+'-'+format.day).toISOString();
    console.log(this.user.birthdate);
  }

  inactivateUser(id_user){
    swal.fire({
      title: 'Advertencia',
      text: "Este usuario será inactivado. ¿Desea continuar?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#5E72E4',
      cancelButtonColor: '#D40B33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.userService.inactivate(id_user).subscribe(user => {
          if (user.id_user) {
            swal.fire({
              title: '¡Listo!',
              text: 'El usuario ha sido inactivado correctamente',
              type: 'success',
              confirmButtonText: 'Ok'
            })
            setTimeout(() => {
              this.getUsers();
            }, 1000);
          } else {
            swal.fire({
              title: '¡Error!',
              text: 'Ha ocurrido un error durante la inactivación del usuario.',
              type: 'error',
              confirmButtonText: 'Ok'
            })
          }
        })
      }
    })
  }
  
  open(modal, modalSize) {
    this.action = 0;
    modalSize = modalSize ? modalSize:'sm';
    this.modalRef = this.modalService.open(modal, {size: modalSize, centered: true});
    this.modalRef.result.then((data) => {}, (reason) => { this.cleanModel() });
  }

  cleanModel(){
    this.user = {
      id_user: '',
      unique_id: '',
      name: '',
      first_ln: '',
      second_ln: '',
      id_course: 0,
      email: '',
      phone: '',
      phone2: '',
      address: '',
      blood_type: 0,
      id_profile: 0,
      photo: null,
      birthdate: '',
      birthdateFormat: '',
      allergies: []
    }
  }

  onPhotoChange(evt: any){
    if (evt.target.files && evt.target.files[0]) {
      this.spinner.show();
      const reader: FileReader = new FileReader();

      reader.readAsDataURL(evt.target.files[0]); // read file as data url

      reader.onload = (evt: any) => { // called once readAsDataURL is completed
        this.user.photo = evt.target.result;
        this.safe_photo = this.sanitizer.bypassSecurityTrustStyle(`url(${evt.target.result})`);
        this.spinner.hide();        
      }
    }
  }

  onFileChange(evt: any) {
    
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length > 1) {
      swal.fire({
        title: 'Error al adjuntar',
        text: 'No puede seleccionar más de un archivo.',
        type: 'warning',
        confirmButtonText: 'Ok'
      })
    } else {
      this.spinner.show();
      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
  
        /* grab first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
  
        /* save data */
        this.data = (XLSX.utils.sheet_to_json(ws, {header: ["id_user", "name", "first_ln", "second_ln", "email", "phone", "phone2", "profile", "course", "address", "birthdate", "blood_type", "unique_id"]}));
      };
      if (target.files[0]){
        reader.readAsBinaryString(target.files[0]);
        this.filename = target.files[0].name;
      }
      
      setTimeout(() => {
        this.spinner.hide();
        this.data.shift();
        this.validateImport();
      }, 1000);
      
    }
  }
  
  validateImport(){
    this.importSuccess = true;
    this.data.map(element => {
      console.log(element.birthdate);
      // Validate profile
      let prof = this.profiles.find(p => {return p.name == element.profile})
      element.id_profile = prof ? prof.id_profile:0;

      // Validate course
      let course = this.courses.find(c => {return c.name == element.course})
      element.id_course = course ? course.id_course:0;

      // Validate duplicated User ID
      let user = this.users.find(u => {return u.id_user == element.id_user})

      if (!prof || !course) { element.invalid = true; this.importSuccess = false };
      if (user) { element.duplicate = true; };

      return element;
    });

    if(!this.importSuccess){
      swal.fire({
        title: '¡Error!',
        text: 'Hubo un error en la lectura del archivo. Por favor verifique la información ingresada.',
        type: 'error',
        confirmButtonText: 'Ok'
      })
    }
    console.log(this.data);
  }

  importUsers(){
      this.spinner.show();
      this.userService.importUsers(this.data).subscribe(response => {
      if (response.success > 0) {
        this.spinner.hide();
        swal.fire({
          title: '¡Listo!',
          text: `Se guardaron ${response.success} usuarios correctamente`,
          type: 'success',
          confirmButtonText: 'Ok'
        })
        this.modalRef.close();
        this.getUsers();
      } else {
        console.log(response.errors);
        this.spinner.hide();
        swal.fire({
          title: '¡Error!',
          text: 'Ha ocurrido un error durante la creación de usuarios',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      }
    })
  }
}
