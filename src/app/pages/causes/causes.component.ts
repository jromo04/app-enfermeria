import { Component, OnInit } from '@angular/core';
import { CausesService } from './../../services/causes.service';
import  swal  from 'sweetalert2';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-causes',
  templateUrl: './causes.component.html',
  styleUrls: ['./causes.component.scss']
})
export class CausesComponent implements OnInit {

  public causes: any;
  public filter_causes: any;
  public cause: any;
  public action = 0;  //   0: Crear; 1: Editar

  
  constructor(private causesService: CausesService) { }
  
  ngOnInit() {
    this.getCauses();
    this.cause = {
      id_cause: 0,
      description: ''
    }
  }
  
  search(text) {
    const term = text.toLowerCase();
    this.filter_causes = this.causes.filter(cause => {
      return cause.cod_cause.toString().includes(term)
          || cause.description.toLowerCase().includes(term);
    });
  }
  
  getCauses(){
    this.causesService.getAll().subscribe(causes => {
      this.causes = causes;
      this.filter_causes = causes;
    })
  }

  getCauseById(id){
    this.action = 1;
    this.cause = this.causes.find(c => { return c.id_cause == id})
  }

  saveCause(){
    if (this.action == 0){
      this.causesService.create(this.cause).subscribe(cause => {
        if(cause.id_cause){
          swal.fire({
            title: '¡Listo!',
            text: 'Causal creado correctamente.',
            type: 'success',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
          this.cause = { id_cause: 0, description: '' }
          this.getCauses();
        } else {
          swal.fire({
            title: '¡Error!',
            text: 'Hubo un error en la creación del causal.',
            type: 'error',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
        }
      })
    } else {
      this.causesService.update(this.cause).subscribe(response => {
        if(response[0] > 0){
          swal.fire({
            title: '¡Listo!',
            text: 'Causal editado correctamente.',
            type: 'success',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
          this.action = 0;
          this.cause = { id_cause: 0, description: '' }
          this.getCauses();
        } else {
          swal.fire({
            title: '¡Error!',
            text: 'Hubo un error en la edición del causal.',
            type: 'error',
            confirmButtonColor: '#5E72E4',
            confirmButtonText: 'Ok'
          })
        }
      })
    }
  }

  deleteCause(id){
    this.causesService.delete(id).subscribe(del => {
      if (del.response > 0) {
        swal.fire({
          title: '¡Listo!',
          text: 'Causal eliminado correctamente.',
          type: 'success',
          confirmButtonColor: '#5E72E4',
          confirmButtonText: 'Ok'
        })
        this.getCauses();
      } else {
        swal.fire({
          title: '¡Error!',
          text: 'Hubo un error en la eliminación del causal.',
          type: 'error',
          confirmButtonColor: '#5E72E4',
          confirmButtonText: 'Ok'
        })
      }
    });
  }
}
