import { Component, OnInit } from '@angular/core';
import { IngresosService } from '../../services/ingresos.service';
import { ClockService } from '../../services/clock.service';
import { MedicinesService } from '../../services/medicines.service';
import { FingerprintService } from '../../services/fingerprint.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from './../../services/auth.service';

import  swal  from 'sweetalert2';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [NgbModal]
})
export class DashboardComponent implements OnInit {

  public defaultImg = 'assets/img/theme/default-profile.png';
  public ingresos: any = [];
  public n_patients: number = 0;
  public last: any;
  public modalRef: any;
  public user_modal: any = {
    id: '',
    name: ''
  }
  public safe_photo: any;
  public medicines: any;
  public dashboard: any = {
    pending: '',
    current_week: '',
    last: '',
    clock: {
      hours: 0,
      minutes: 0,
      day: 0,
      month: '',
      period: '',
      year: ''
    }
  }

  
  constructor(private ingresosService: IngresosService, 
              private clockService: ClockService,
              private modalService: NgbModal,
              private medicinesService: MedicinesService,
              private fingerprintService: FingerprintService,
              private spinner: NgxSpinnerService,
              private sanitizer: DomSanitizer,
              private authService: AuthService,
              private router:Router) { }
  
  ngOnInit() {
    if (this.authService.loggedIn()){
      this.reloj();
      this.getAllIngresos();
      this.getMedicines();
      // this.fingerprintService.checkin().subscribe(data => {
      //   console.log("Data from fingerprint:", data);
      // })
    } else {
      this.router.navigate(['/login']);
    }
  }

  reloj(){
    this.clockService.getDateTime().subscribe(dateTime => {
      this.dashboard.clock = {
        hours: dateTime.hours,
        minutes: dateTime.minutes,
        seconds: dateTime.seconds,
        period: dateTime.period,
        day: dateTime.day,
        day_month: dateTime.day_month,
        year: dateTime.year
      }
    });
  }

  open(modal, modalSize) {
    modalSize = modalSize ? modalSize:'sm';
    this.modalRef = this.modalService.open(modal, {size: modalSize, centered: true});
    this.modalRef.result.then((data) => {}, (reason) => { this.cleanModel() });
  }

  getAllIngresos(){
    this.ingresosService.getAll().subscribe(ingresos => {
      this.ingresos = ingresos.map(i => {
        const n_ingreso = {
          id_ingreso: i.id_ingreso,
          id_student: i.id_student,
          notified: i.notified,
          start_time: i.start_time,
          end_time: i.end_time,
          status: i.status,
          details: i.details,
          student: {
            name: i.student.name,
            first_ln: i.student.first_ln,
            second_ln: i.student.second_ln,
            course: i.student.course,
            save_photo: i.student.photoImg ? this.sanitizer.bypassSecurityTrustStyle(`url(${i.student.photoImg})`) : `url(${this.defaultImg})`
          }
        }
        return n_ingreso;
      });

      // Update Dashboard
      this.dashboard.pending = this.ingresos.filter(el => el.status == 'En espera').length;
      this.dashboard.last = _.maxBy(this.ingresos, el => el.start_time);
      this.dashboard.current_week = _.filter(this.ingresos, (el) => moment(el.start_time).week() == moment().week()).length;
      // console.log(cant);
    })
  }

  alta(id){
    let ingreso = {
      id_ingreso: id,
      status: 'Atendido',
      end_time: new Date()
    };
    this.ingresosService.update(ingreso).subscribe(ingresos => {
      console.log(ingresos);
    })
    setTimeout(()=>{
      this.getAllIngresos();
    },200)
  }

  ingresar(id_student, route){
    //Validate duplicate
    let patient = this.ingresos.find(el => {return el.id_student == id_student})
    if (patient && patient.status != 'Atendido'){
      swal.fire({
        title: '¡Error!',
        text: 'El estudiante ya se encuentra en la lista de enfermería',
        type: 'error',
        confirmButtonText: 'Ok'
      })
    } else {
      let ingreso = {
        id_student: id_student,
        status: 'En espera'
      }
      
      this.ingresosService.create(ingreso).subscribe(ingreso =>{
        if(ingreso.id_ingreso){
          if (route == "/treatment/"){
            this.router.navigate([route, ingreso.id_ingreso, id_student]);
          } else {
            this.router.navigate([route, id_student]);
          }
        } else {
          swal.fire({
            title: '¡Error!',
            text: 'Error en la creación del ingreso',
            type: 'error',
            confirmButtonText: 'Ok'
          })
        }
      });
    }
  }

  eliminar(id){
    swal.fire({
      title: 'Advertencia',
      text: "Este usuario será eliminado de la lista. ¿Desea continuar?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#5E72E4',
      cancelButtonColor: '#D40B33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'Cancelar',
      reverseButtons: true
    })
    .then(result => {
      if (result.value) {
        this.ingresosService.delete(id).subscribe(del =>{
          if (del.response > 0) {
            swal.fire({
              title: '¡Listo!',
              text: 'Ingreso eliminado correctamente.',
              type: 'success',
              confirmButtonColor: '#5E72E4',
              confirmButtonText: 'Ok'
            })
            this.getAllIngresos();
          } else {
            swal.fire({
              title: '¡Error!',
              text: 'Hubo un error en la eliminación del ingreso.',
              type: 'error',
              confirmButtonColor: '#5E72E4',
              confirmButtonText: 'Ok'
            })
          }
        });
      }
    })
  }

  getMedicines(){
    this.medicinesService.getAll().subscribe(medicines =>{
      this.medicines = medicines;
    })
  }

  notify(id){
    this.spinner.show();
    this.ingresosService.notify(id).subscribe(response => {
      this.spinner.hide();
      if (response.status) {
        swal.fire({
          title: '¡Listo!',
          text: 'Notificaciones enviadas satisfactoriamente.',
          type: 'success',
          confirmButtonText: 'Ok'
        })
        this.getAllIngresos();
      } else {
        swal.fire({
          title: '¡Error!',
          text: 'Error en el envío de las notificaciones.',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      }
    })
  }

  cleanModel(){
    this.user_modal = {
      id: '',
      name: ''
    }
  }

}
