import { Component, OnInit } from '@angular/core';
import { CausesService } from '../../services/causes.service';
import { MedicinesService } from '../../services/medicines.service';
import { IngresosService } from '../../services/ingresos.service';
import { ActivatedRoute, Router } from '@angular/router';
import  swal  from 'sweetalert2';

declare var $: any;

@Component({
  selector: 'app-treatment',
  templateUrl: './treatment.component.html',
  styleUrls: ['./treatment.component.scss']
})
export class TreatmentComponent implements OnInit {

  public details: any[] = [
    {
      id_ingreso: 0,
      id_cause: 0,
      cause: '',
      id_medicine: 0,
      medicine: '',
      qty: 0,
      comment: ''
    }];
  public id_student: number;
  public id_treatment: number;
  public causes: any[] = [];
  public ingreso: any = {};
  public medicines: any[] = [];

  constructor(private causesService: CausesService, 
              private medicinesService: MedicinesService, 
              private ingresosService: IngresosService, 
              private urlRoute: ActivatedRoute,
              private router:Router) { }

  ngOnInit() {

    this.getCauses();
    this.getMedicines();
    this.urlRoute.params.subscribe(params => {
      this.id_student = params['id_user'];
      this.id_treatment = params['id_treatment'];
      this.getIngresoById(this.id_treatment);
    })
  }

  getIngresoById(id_ingreso){
    this.ingresosService.getById(id_ingreso).subscribe(ingreso =>{
      this.ingreso = ingreso;
      if (ingreso.details.length > 0){
        this.details = ingreso.details.map(detail => {
          const det = {
            id_ingreso: detail.id_ingreso,
            id_ingreso_det: detail.id_ingreso_det,
            id_cause: detail.id_cause,
            cause: detail.cause.description,
            id_medicine: detail.id_medicine,
            medicine: detail.medicine.name,
            qty: detail.qty,
            comment: detail.comment
          }
        return det;
        });
      }
    })
    console.log(this.details);
  }

  saveIngreso(){
    let ingreso = {
      id_ingreso: this.id_treatment,
      id_student: this.id_student,
      details: this.details,
      status: 'En tratamiento'
    }
    
    console.log(ingreso)
    this.ingresosService.update(ingreso).subscribe(ingreso =>{
      if(ingreso.id_ingreso){
        swal.fire({
          title: '¡Listo!',
          text: 'Tratamiento aplicado correctamente',
          type: 'success',
          confirmButtonText: 'Ok'
        })
        .then(() => {
          this.router.navigate(['dashboard']);
        })
      } else {
        swal.fire({
          title: 'Error!',
          text: 'Error en la creación del tratamiento',
          type: 'error',
          confirmButtonText: 'Ok'
        })
      }
    });
    
  }

  getCauses(){
    this.causesService.getAll().subscribe(causes =>{
      this.causes = causes;
    })
  }

  getMedicines(){
    this.medicinesService.getAll().subscribe(medicines =>{
      this.medicines = medicines;
    })
  }

  setCause(i, cause){
    console.log(cause)
    this.details[i].cause = cause;
  }

  setMedicine(i, medicine){
    this.details[i].medicine = medicine;
  }

  addCause(){
    let cause = {
      id_cause: 0,
      cause: '',
      // id_ingreso: this.id_treatment,
      id_medicine: 0,
      medicine: '',
      qty: 0,
      comment: ''
    };
    this.details.push(cause);
  }

  deleteCause(element){
    this.details.splice(element, 1);
  }

}
