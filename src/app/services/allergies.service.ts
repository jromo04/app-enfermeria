import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AllergiesService {

  constructor(private http: HttpClient, private authService: AuthService) { }
  
  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/allergies/findAll", this.httpOptions);
  }

  getById(id_allergy):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/allergies/"+id_allergy+"/findByPk", this.httpOptions);
  }

  create(allergy): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/allergies/create", allergy, this.httpOptions);
  }

  update(allergy): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/allergies/update", allergy, this.httpOptions);
  }
}
