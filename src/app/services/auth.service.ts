import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from './../global';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token: string;
  constructor(private http: HttpClient) { }

  login(user):Observable<any>{
    return this.http.post(GLOBAL.API_URL + "/api/auth/login", user).pipe(
      tap((res) => {
        if (res.token) {
          localStorage.setItem('ACCESS_TOKEN', res.token);
          this.token = res.token;
        }
      })
    );
  }

  logout(){
    this.token = '';
    localStorage.removeItem('ACCESS_TOKEN');
  }

  public getToken(): string {
    if(!this.token){
      this.token = localStorage.getItem('ACCESS_TOKEN');
    }
    return this.token;
  }

  public loggedIn(): boolean {
    return localStorage.getItem('ACCESS_TOKEN') !==  null;
  }
}
