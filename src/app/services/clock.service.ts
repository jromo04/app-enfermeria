import { Injectable } from '@angular/core';
import {timer, Observable, Subject} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ClockService {

  clock: Observable <Date>;
  dateTime = new Subject<any>();
  vr: any = {};

  constructor() { 
    this.clock = timer(0,1000).pipe(map(t => new Date()),shareReplay(1));
  }

  getDateTime(): Observable<any>{
    this.clock.subscribe(t => {
      this.vr = {
        hours: ((t.getHours() % 12) ? (t.getHours() % 12) : 12),
        minutes: (t.getMinutes() < 10) ? '0' + t.getMinutes() : t.getMinutes().toString(),
        period: t.getHours() > 11 ? 'PM' : 'AM',
        day_month: t.toLocaleString('es-CO', { day: '2-digit', month: 'long' }).replace('.', '').replace('-', ' '),
        day: t.toLocaleString('es-CO', { weekday: 'long' }).replace('.', ''),
        seconds: t.getSeconds() < 10 ? '0' + t.getSeconds() : t.getSeconds().toString(),
        year: t.getFullYear()
      }
      this.dateTime.next(this.vr);
    });
    return this.dateTime.asObservable();
  }

}
