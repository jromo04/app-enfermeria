import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BodegaService {
  
  constructor(private http: HttpClient, private authService: AuthService) { }

  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/bodega/findAll", this.httpOptions);
  }

  getById(id_medicine):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/bodega/"+id_medicine+"/findByPk", this.httpOptions);
  }

  create(medicine): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/bodega/create", medicine, this.httpOptions);
  }

  update(medicine): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/bodega/update", medicine, this.httpOptions);
  }
}
