import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class IngresosService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/ingresos/findAll", this.httpOptions);
  }

  getById(id_ingreso):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/ingresos/"+id_ingreso+"/findByPk", this.httpOptions);
  }

  getByUser(id_user):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/ingresos/"+id_user+"/findByUser", this.httpOptions);
  }

  create(ingreso): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/ingresos/create", ingreso, this.httpOptions);
  }

  update(ingreso): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/ingresos/update", ingreso, this.httpOptions);
  }

  updateDetail(detail): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/ingresos/update-detail", detail, this.httpOptions);
  }

  notify(id_ingreso):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/ingresos/"+id_ingreso+"/notify", this.httpOptions);
  }

  delete(id_ingreso): Observable<any>{
    return this.http.get<any>(GLOBAL.API_URL + "/api/ingresos/"+id_ingreso+"/delete", this.httpOptions);
  }
}
