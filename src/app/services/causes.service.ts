import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CausesService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/causes/findAll", this.httpOptions);
  }

  getById(id_cause):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/causes/"+id_cause+"/findByPk", this.httpOptions);
  }

  create(cause): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/causes/create", cause, this.httpOptions);
  }

  update(cause): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/causes/update", cause, this.httpOptions);
  }

  delete(id_cause): Observable<any>{
    return this.http.get<any>(GLOBAL.API_URL + "/api/causes/"+id_cause+"/delete", this.httpOptions);
  }
}
