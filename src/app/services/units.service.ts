import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UnitsService {

  constructor(private http: HttpClient, private authService: AuthService) { }
  
  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/units/findAll", this.httpOptions);
  }

  getById(id_unit):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/units/"+id_unit+"/findByPk", this.httpOptions);
  }

  create(unit): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/units/create", unit, this.httpOptions);
  }

  update(unit): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/units/update", unit, this.httpOptions);
  }
}
