import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserProfilesService {

  constructor(private http: HttpClient, private authService: AuthService) { }
  
  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/user_profiles/findAll", this.httpOptions);
  }

  getById(id_profile):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/user_profiles/"+id_profile+"/findByPk", this.httpOptions);
  }

  create(user): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/user_profiles/create", user, this.httpOptions);
  }

  update(user): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/user_profiles/update", user, this.httpOptions);
  }
}
