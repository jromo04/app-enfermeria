import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/courses/findAll", this.httpOptions);
  }

  getById(id_course):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/courses/"+id_course+"/findByPk", this.httpOptions);
  }

  create(course): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/courses/create", course, this.httpOptions);
  }

  update(course): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/courses/update", course, this.httpOptions);
  }
}
