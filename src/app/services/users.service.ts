import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, private authService: AuthService) { }

	private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/users/findAll", this.httpOptions);
  }

  getById(id_user):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/users/"+id_user+"/findByPk", this.httpOptions);
  }

  getByCriteria(criteria):Observable<any>{
    return this.http.post(GLOBAL.API_URL + "/api/users/findByCriteria", criteria, this.httpOptions);
  }

  create(user): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/users/create", user, this.httpOptions);
  }

  update(user): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/users/update", user, this.httpOptions);
  }

  inactivate(id_user): Observable<any>{
    return this.http.get<any>(GLOBAL.API_URL + "/api/users/"+id_user+"/inactivate", this.httpOptions);
  }

  importUsers(users): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/users/importUsers", users, this.httpOptions);
  }
}
