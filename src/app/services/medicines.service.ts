import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from '../global';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class MedicinesService {

  constructor(private http: HttpClient, private authService: AuthService) { }
  
  private httpOptions = {
		headers: new HttpHeaders({
    		'Content-Type':  'application/json',
    		'x-access-token': this.authService.getToken()
  		})
  };

  getAll():Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/medicines/findAll", this.httpOptions);
  }

  getById(id_medicine):Observable<any>{
    return this.http.get(GLOBAL.API_URL + "/api/medicines/"+id_medicine+"/findByPk", this.httpOptions);
  }

  create(medicine): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/medicines/create", medicine, this.httpOptions);
  }

  update(medicine): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/medicines/update", medicine, this.httpOptions);
  }

  inactivate(id_medicine): Observable<any>{
    return this.http.get<any>(GLOBAL.API_URL + "/api/medicines/"+id_medicine+"/inactivate", this.httpOptions);
  }

  importMedicines(medicines): Observable<any>{
    return this.http.post<any>(GLOBAL.API_URL + "/api/medicines/importMedicines", medicines, this.httpOptions);
  }
}
