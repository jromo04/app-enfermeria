import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { UsersComponent } from '../../pages/users/users.component';
import { TreatmentComponent } from '../../pages/treatment/treatment.component';
import { BodegaComponent } from '../../pages/bodega/bodega.component';
import { CausesComponent } from '../../pages/causes/causes.component';
import { AccidentReportComponent } from '../../pages/accident-report/accident-report.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile/:id_user', component: UserProfileComponent },
    { path: 'users', component: UsersComponent },
    { path: 'bodega', component: BodegaComponent },
    { path: 'causes', component: CausesComponent },
    { path: 'treatment/:id_treatment/:id_user', component: TreatmentComponent },
    { path: 'accident-report', component: AccidentReportComponent }
];
