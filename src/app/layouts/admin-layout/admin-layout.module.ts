import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../../pipes/pipes.module';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { SelectDropDownModule } from 'ngx-select-dropdown'
// import { ToastrModule } from 'ngx-toastr';

import { UsersComponent } from '../../pages/users/users.component';
import { TreatmentComponent } from '../../pages/treatment/treatment.component';
import { BodegaComponent } from '../../pages/bodega/bodega.component';
import { CausesComponent } from '../../pages/causes/causes.component';
import { AccidentReportComponent } from '../../pages/accident-report/accident-report.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    NgxSpinnerModule,
    PipesModule,
    UiSwitchModule,
    SelectDropDownModule,
    TranslateModule.forRoot()
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    UsersComponent,
    BodegaComponent,
    CausesComponent,
    TreatmentComponent,
    AccidentReportComponent
  ]
})

export class AdminLayoutModule {}
